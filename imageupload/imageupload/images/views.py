from rest_framework import viewsets, permissions
from .models import Image
from .serializers import ImageSerializer

class ImageViewSet(viewsets.ModelViewSet):
    queryset = Image.objects.all()
    serializer_class = ImageSerializer
    permission_classes = [permissions.AllowAny]

    def perform_destroy(self, instance):
        # Видаляємо файл з файлової системи при видаленні об'єкту Image
        instance.file.delete()
        instance.delete()
